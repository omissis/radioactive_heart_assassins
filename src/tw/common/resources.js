/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Radioactive Heart Assassins Level 000 Resources file
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */

_.ns('Tw.Common');

Tw.Common.Resources =
    new Tw.Resources([
        //image
        {
            type : "image",
            src : "res/background.png",
            id : "common-background"
        },

        //image - villains
        {
            type : "image",
            src : "res/actors/villain-bacterium.png",
            id : 'villain-bacterium'
        },

        {
            type : "image",
            src : "res/actors/villain-drug.png",
            id : 'villain-drug'
        },

        {
            type : "image",
            src : "res/actors/villain-radioactive.png",
            id : 'villain-radioactive'
        },

        {
            type : "image",
            src : "res/actors/villain-virus.png",
            id : 'villain-virus'
        },

        //image - weapons
        {
            type : "image",
            src : "res/weapons/weapon-antibodies.png",
            id : 'weapon-antibodies' // against villain-bacterium
        },

        {
            type : "image",
            src : "res/weapons/weapon-medicin.png",
            id : 'weapon-medicin' // against villain-radioactive
        },

        {
            type : "image",
            src : "res/weapons/weapon-oxygen.png",
            id : 'weapon-oxygen' // against villain-drug
        },

        {
            type : "image",
            src : "res/weapons/weapon-vaccine.png",
            id : 'weapon-vaccine' // against villain-virus'
        },

        //plist

        //fnt

        //tmx
        {
            type : "tmx",
            src : "res/level-000.tmx",
            id : "level-000"
        },

        {
            type : "tmx",
            src : "res/level-001.tmx",
            id : "level-001"
        }

        //bgm

        //effect
    ]);
