/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Radioactive Heart Assassins Level 000 Config file
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */


_.ns('Tw.Common');

Tw.Common.Config = {
    cellWidth : 32,
    cellHeight : 32,
    halfCellWidth : 16,
    halfCellHeight : 16
};
