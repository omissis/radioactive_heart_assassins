_.ns('Tw.Common');

(function (Tw, cc, clearTimeout) {
    "use strict";

    Tw.Common.Factory = {
        getElementFromPortalAnimation : function (obj) {
            var HCW = Tw.Common.Config.halfCellWidth,
                HCH = Tw.Common.Config.halfCellHeight,
                CW = Tw.Common.Config.cellWidth,
                CH = Tw.Common.Config.cellHeight,
                oldX = obj.x / CW,
                oldY = obj.y / CH,
                steps = obj.steps.split(";");

            var animations = [];
            for (var j = 0, len2 = steps.length; j < len2; ++j) {
                var step = steps[j].split(","),
                    newX = parseInt(step[0], 10),
                    newY = parseInt(step[1], 10),
                    speed = Tw.getSpeed({x : newX, y : newY}, {x : oldX, y : oldY});

                animations.push(
                    [speed, cc.p(step[0] * CW + HCW, step[1] * CH + HCH)]
                );

                // store the values for later
                oldX = newX;
                oldY = newY;
            }

            return animations;
        },
        stopFactory : function (elementsTimeouts) {
            for (var i = 0, len = elementsTimeouts.length; i < len; ++i) {
                clearTimeout(elementsTimeouts[i]);
            }
        }
    };
}(window.Tw, window.cc, window.clearTimeout));
