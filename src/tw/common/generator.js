_.ns('Tw.Common');

/**
 * Use this object to generate elements or waves of elements
 *
 * @todo use _.extend for options
 */
Tw.Common.Generator = function (elementTypes, options) {
    "use strict";

    if (!elementTypes) {
        throw "you must provide an array containing values representing your element types";
    }

    options = options || {};

    var minIntervalBetweenWaves = options.minIntervalBetweenWaves || 4,
        maxIntervalBetweenWaves = options.maxIntervalBetweenWaves || 10,
        maxIntervalBetweenElements = options.maxIntervalBetweenElements || 2,
        minIntervalBetweenElements = options.minIntervalBetweenElements || 1,
        maxElementsPerWave = options.maxElementsPerWave || 10,
        minElementsPerWave = options.minElementsPerWave || 1;

    var generateElement = function (type) {
            if (!_.isNumber(type)) {
                type = Math.floor(Math.random() * elementTypes.length);
            }
            return [
                elementTypes[type],
                Math.max(minIntervalBetweenElements, Math.floor(Math.random()) * maxIntervalBetweenElements)
            ];
        },
        generateWave = function (nElements) {
            nElements = nElements || 0;
            var elements = [];
            for (var i = 0; i < nElements; ++i) {
                elements[i] = generateElement();
            }
            return elements;
        },
        generateWaves = function (nWaves) {
            nWaves = nWaves || 0;
            var waves = [];
            for (var i = 0; i < nWaves; ++i) {
                waves[i] = generateWave(
                    Math.max(minElementsPerWave, Math.floor(Math.random() * maxElementsPerWave)),
                    Math.max(minIntervalBetweenWaves, Math.floor(Math.random() * maxIntervalBetweenWaves))
                );
            }
            return waves;
        };

    return {
        generateElement : generateElement,
        generateWave : generateWave,
        generateWaves : generateWaves
    };
};
