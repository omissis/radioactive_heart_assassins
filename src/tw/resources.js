/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Radioactive Heart Assassins Resources file
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */

_.ns('Tw');

/**
 * Factory object for building resources objects
 */
Tw.Resources = (function (_) {
    "use strict";

    var _resources = [];

    var getResourcesByType = function (type) {
        var resourcesByType = [];

        for (var i = 0, len = _resources.length; i < len; ++i) {
            if (_resources[i].type === type) {
                resourcesByType.push(_resources[i]);
            }
        }

        return resourcesByType;
    };

    var getResourceByIdAndType = function (id, type) {
        for (var i = 0, len = _resources.length; i < len; ++i) {
            if (_resources[i].id === id) {
                if (type && type === _resources[i].type) {
                    return _resources[i];
                }
                return null;
            }
        }

        return null;
    };

    return function (resources) {
        resources = resources || [];

        if (!_.isArray(resources)) {
            throw "resources must be an array";
        }

        _resources = resources;

        return {
            getAll : function () {
                return _resources;
            },

            // Get resources by type methods

            getImages : function () {
                return getResourcesByType('image');
            },
            getPlists : function () {
                return getResourcesByType('plist');
            },
            getFnts : function () {
                return getResourcesByType('fnt');
            },
            getTmxs : function () {
                return getResourcesByType('tmx');
            },
            getBgms : function () {
                return getResourcesByType('bgm');
            },
            getEffects : function () {
                return getResourcesByType('effect');
            },

            // Get single resource by id and type methods

            getImage : function (id) {
                return getResourceByIdAndType(id, 'image');
            },
            getPlist : function (id) {
                return getResourceByIdAndType(id, 'plist');
            },
            getFnt : function (id) {
                return getResourceByIdAndType(id, 'fnt');
            },
            getTmx : function (id) {
                return getResourceByIdAndType(id, 'tmx');
            },
            getBgm : function (id) {
                return getResourceByIdAndType(id, 'bgm');
            },
            getEffect : function (id) {
                return getResourceByIdAndType(id, 'effect');
            }
        };
    };
}(_));
