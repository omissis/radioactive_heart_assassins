_.ns('Tw.Weapons');

// TODO kill this var
var SPEED = 8.0;

(function (Tw, setTimeout, clearTimeout, _) {
    "use strict";

    Tw.Weapons.WeaponFactory = function (cc, Tw, scene, obj) {
        if (!cc) throw "no cocos2d";
        if (!Tw) throw "no Tw";
        if (!scene) throw "no scene";
        if (!obj) throw "no obj";

        var weaponsTimeouts = [],
            lastPoint = null,
            finalPoint = null;

        var getWeapons = function () {
                var sceneChildren = scene.getChildren(),
                    children = [];

                for (var i = 0, len = sceneChildren.length; i < len; ++i) {
                    if (sceneChildren[i].getTag() === "weapon") {
                        children.push(sceneChildren[i]);
                    }
                }

                return children;
            },
            stopFactory = function () {
                for (var i = 0, len = weaponsTimeouts.length; i < len; ++i) {
                    clearTimeout(weaponsTimeouts[i]);
                    weaponsTimeouts[i] = null;
                }
                weaponsTimeouts = [];
            },
            createWeapons = function () {
                for (var i = 0; i < 6; ++i) {
                    var wt = Tw.Weapons.WeaponGenerator.generateWeaponAndTimeout(obj);

                    scene.addChild(wt.weapon);

                    wt.weapon.setPosition(96 + i * 96 + 16, finalPoint.y);
                }
            },
            updateWeapons = function () {
                stopFactory();

                var children = getWeapons();

                for (var i = 0, len = children.length; i < len; ++i) {
                    children[i].stopAllActions();

                    lastPoint = cc.p(finalPoint.x + 96 * i, lastPoint.y);

                    children[i].runAction(cc.MoveTo.create(1.0, lastPoint));
                }

                var wt = Tw.Weapons.WeaponGenerator.generateWeaponAndTimeout(obj);

                scene.addChild(wt.weapon);

                lastPoint = cc.p(finalPoint.x + 96 * i, lastPoint.y);

                wt.weapon.runAction(cc.MoveTo.create(1.0, lastPoint));
            };

        var uiBackside = cc.Sprite.create("res/ui-backside.png");
        uiBackside.setPosition(cc.p(625, 80));

        scene.addChild(uiBackside, 99);

        return {
            start : function (finalPosition) {
                finalPoint = cc.p(finalPosition.x, finalPosition.y);
                lastPoint = finalPoint;
                createWeapons();
            },
            stop : stopFactory,
            removeAllChildren : function () {
                var child = null;

                stopFactory();

                while ((child = scene.getChildByTag('weapon'))) {
                    scene.removeChild(child);
                }
            },
            removeChild : function (child) {
                scene.removeChild(child);

                updateWeapons();
            },
            getChildren : getWeapons
        };
    };
}(window.Tw, window.setTimeout, window.clearTimeout, window._));
