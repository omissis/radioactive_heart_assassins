_.ns('Tw.Weapons');

Tw.Weapons.Vaccine = function (position) {
    var Weapon = new Tw.Weapons.Weapon(position, 'weapon-vaccine');

    Weapon.tid = Tw.Weapons.T_VACCINE;
    Weapon.against = Tw.Actors.T_VIRUS;

    return Weapon;
};

Tw.Weapons.T_VACCINE = 3;
