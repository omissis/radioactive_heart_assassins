_.ns('Tw.Weapons');

Tw.Weapons.Weapon = function (position, image) {
    var Weapon = cc.Sprite.extend({
        tid : null,
        against : null
    });

    var ret = new Weapon();

    ret.initWithFile(Tw.Common.Resources.getImage(image).src);
    ret.setPosition(position);
    ret.setTag('weapon');

    return ret;
};
