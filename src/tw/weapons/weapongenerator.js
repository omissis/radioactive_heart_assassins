_.ns('Tw.Weapons');

(function (Tw, cc) {
    "use strict";

    Tw.Weapons.WeaponGenerator = new Tw.Common.Generator(
        [
            Tw.Weapons.T_ANTIBODIES,
            Tw.Weapons.T_MEDICIN,
            Tw.Weapons.T_OXYGEN,
            Tw.Weapons.T_VACCINE
        ],
        {
            minIntervalBetweenElements : 2,
            maxIntervalBetweenElements : 5
        }
    );

    Tw.Weapons.WeaponGenerator.getWeaponByType = function (type) {
        if (Tw.Weapons.T_ANTIBODIES === type) {
            return Tw.Weapons.Antibodies;
        }

        if (Tw.Weapons.T_MEDICIN === type) {
            return Tw.Weapons.Medicin;
        }

        if (Tw.Weapons.T_OXYGEN === type) {
            return Tw.Weapons.Oxygen;
        }

        if (Tw.Weapons.T_VACCINE === type) {
            return Tw.Weapons.Vaccine;
        }

        throw "given weapon type '" + type + "' does not exists";
    };

    Tw.Weapons.WeaponGenerator.generateWeapon = function (obj, type) {
        var element = Tw.Weapons.WeaponGenerator.generateElement(type),
            WeaponCtor = Tw.Weapons.WeaponGenerator.getWeaponByType(element[0]),
            startingPoint = cc.p(obj.x, obj.y);

        return new WeaponCtor(startingPoint);
    };

    Tw.Weapons.WeaponGenerator.generateWeaponAndTimeout = function (obj) {
        var element = Tw.Weapons.WeaponGenerator.generateElement(),
            WeaponCtor = Tw.Weapons.WeaponGenerator.getWeaponByType(element[0]),
            startingPoint = cc.p(
                obj.x + 2 * Tw.Common.Config.cellWidth,
                obj.y + 1.5 * Tw.Common.Config.cellHeight
            );

        return {
            weapon : new WeaponCtor(startingPoint),
            timeout : element[1]
        };
    };
}(window.Tw, window.cc));
