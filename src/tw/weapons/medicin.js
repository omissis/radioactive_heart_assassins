_.ns('Tw.Weapons');

Tw.Weapons.Medicin = function (position) {
    var Weapon = new Tw.Weapons.Weapon(position, 'weapon-medicin');

    Weapon.tid = Tw.Weapons.T_MEDICIN;
    Weapon.against = Tw.Actors.T_RADIOACTIVE;

    return Weapon;
};

Tw.Weapons.T_MEDICIN = 1;
