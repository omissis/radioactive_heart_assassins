_.ns('Tw.Weapons');

Tw.Weapons.Oxygen = function (position) {
    var Weapon = new Tw.Weapons.Weapon(position, 'weapon-oxygen');

    Weapon.tid = Tw.Weapons.T_OXYGEN;
    Weapon.against = Weapon.against = Tw.Actors.T_DRUG;

    return Weapon;
};

Tw.Weapons.T_OXYGEN = 2;
