_.ns('Tw.Weapons');

Tw.Weapons.Antibodies = function (position) {
    var Weapon = new Tw.Weapons.Weapon(position, 'weapon-antibodies');

    Weapon.tid = Tw.Weapons.T_ANTIBODIES;
    Weapon.against = Tw.Actors.T_BACTERIUM;

    return Weapon;
};

Tw.Weapons.T_ANTIBODIES = 0;
