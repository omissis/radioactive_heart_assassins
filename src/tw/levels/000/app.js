/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Radioactive Heart Assassins Level 000 App file
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */

_.ns('Tw.Levels.000');

/**
 * Level 000 App object
 */
Tw.Levels['000'].App = (function (cc, Tw) {
    "use strict";

    var layer = null;

    var MyLayer = cc.Layer.extend({
        backgroundImage : null,
        map : null,
        lazyLayer : null,
        mapObjects : null,

        init : function () {
            this._super();

            var size = cc.Director.getInstance().getWinSize();

            this.lazyLayer = new cc.LazyLayer();

            this.addChild(this.lazyLayer);

            this.initBackgroundImage(size);
            this.initMap();

            this.setMouseEnabled(true);

            return true;
        },

        initBackgroundImage : function (size) {
            this.backgroundImage = cc.Sprite.create("res/background-menu.png");
            this.backgroundImage.setAnchorPoint(cc.p(0.5, 0.465));
            this.backgroundImage.setPosition(cc.p(size.width / 2, size.height / 2));

            this.lazyLayer.addChild(this.backgroundImage, 0);
        },

        initMap : function () {
            this.map = cc.TMXTiledMap.create("res/level-000.tmx");
            this.mapObjects = this.map.getObjectGroup("Objects");
            this.lazyLayer.addChild(this.map, 10);
        },

        onMouseDown:function (e) {
            var loc = e.getLocation(),
                objs = this.mapObjects.getObjects();

            for (var i = 0, len = objs.length; i < len; ++i) {
                var button = objs[i],
                    x = button.x,
                    y = button.y,
                    w = button.width,
                    h = button.height;

                if (loc.x >= x && loc.x <= x + w) {
                    if (loc.y >= y && loc.y <= y + h) {
                        Tw.gotoNextLevel();
                    }
                }
            }
        }
    });

    var MyScene = cc.Scene.extend({
        onEnter :function () {
            this._super();

            layer = new MyLayer();

            this.addChild(layer);

            layer.init();
        }
    });

    return {
        layers : [
            MyLayer
        ],
        scenes : [
            MyScene
        ]
    };
}(window.cc, window.Tw));
