/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Radioactive Heart Assassins Level Winner App file
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */

_.ns('Tw.Levels.Winner');

/**
 * Level Winner App object
 */
Tw.Levels['Winner'].App = (function (cc, Tw) {
    "use strict";

    var layer = null;

    var MyLayer = cc.Layer.extend({
        backgroundImage : null,
        map : null,
        lazyLayer : null,
        mapObjects : null,

        init : function () {
            this._super();

            var size = cc.Director.getInstance().getWinSize();

            this.lazyLayer = new cc.LazyLayer();

            this.addChild(this.lazyLayer);

            this.initBackgroundImage(size);

            this.setMouseEnabled(true);

            return true;
        },

        initBackgroundImage : function (size) {
            this.backgroundImage = cc.Sprite.create("res/background-winner.png");
            this.backgroundImage.setAnchorPoint(cc.p(0.5, 0.465));
            this.backgroundImage.setPosition(cc.p(size.width / 2, size.height / 2));

            this.lazyLayer.addChild(this.backgroundImage, 0);
        },

        onMouseDown:function (e) {
            Tw.gotoFirstLevel();
        }
    });

    var MyScene = cc.Scene.extend({
        onEnter :function () {
            this._super();

            layer = new MyLayer();

            this.addChild(layer);

            layer.init();
        }
    });

    return {
        layers : [
            MyLayer
        ],
        scenes : [
            MyScene
        ]
    };
}(window.cc, window.Tw));
