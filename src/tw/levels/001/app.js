/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Radioactive Heart Assassins Level 001 App file
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */

_.ns('Tw.Levels.001');

/**
 * Level 001 App object
 */
Tw.Levels['001'].App = (function (cc, Tw) {
    "use strict";

    var layer = null,
        villainFactories = [],
        weaponFactory = null,
        dropAreas = [],
        selectedElement,
        selectedElementIndex,
        selectedElementSprite,
        stoppedFactories = 0,
        defeatedFactories = 0;

    var getPortalIdFromName = function (portalName) {
            var nameParts = portalName.split('-');

            return parseInt(nameParts[2], 10);
        },
        setupPortalVillain = function (obj, scene) {
            var portalId = getPortalIdFromName(obj.name);

            villainFactories[portalId] = new Tw.Actors.VillainFactory(cc, Tw, scene, obj, portalId);
        },
        setupPortalHeart = function (obj) {
            return {
                x : obj.x + obj.width / 2,
                y : obj.y + obj.height / 2
            };
        },
        setupPortalWeapon = function (obj, scene) {
            return new Tw.Weapons.WeaponFactory(cc, Tw, scene, obj);
        },
        setupWallWeapon = function (obj) {
            return {
                x : obj.x + obj.width + 1.5 * Tw.Common.Config.cellWidth,
                y : obj.y + obj.height / 2
            };
        },
        setupMapObjects = function (mapObjects, scene) {
            var i, len,
                heart = new Tw.Actors.Heart(10),
                finalWeaponsPosition = { x : null, y : null },
                finalVillainsPosition = { x : null, y : null },
                finalVillainsPositionCallback = function (points) {
                    heart.onHit(points);
                };

            for (i = 0, len = mapObjects.length; i < len; ++i) {
                var obj = mapObjects[i];

                if (obj.type === 'portal-villain') {
                    setupPortalVillain(obj, scene);
                    continue;
                }

                if (obj.type === 'portal-heart') {
                    finalVillainsPosition = setupPortalHeart(obj);
                    continue;
                }

                if (obj.type === 'portal-weapon') {
                    weaponFactory = setupPortalWeapon(obj, scene);
                    continue;
                }

                if (obj.type === 'wall-weapon') {
                    finalWeaponsPosition = setupWallWeapon(obj);
                    continue;
                }

                if (obj.type === 'drop-area') {
                    dropAreas.push(obj);
                    continue;
                }
            }

            // starts the villain factories
            for (i = 0, len = villainFactories.length; i < len; ++i) {
                var villainFactory = villainFactories[i];
                villainFactory.start(5, finalVillainsPosition, finalVillainsPositionCallback);
            }

            // starts the weapon factory
            weaponFactory.start(finalWeaponsPosition);
        };

    var MyLayer = cc.Layer.extend({
        backgroundImage : null,
        map : null,
        lazyLayer : null,
        mapObjects : null,

        init : function () {
            this._super();

            var size = cc.Director.getInstance().getWinSize();

            this.lazyLayer = new cc.LazyLayer();

            this.addChild(this.lazyLayer);

            this.initBackgroundImage(size);
            this.initMap();
            this.initHeart();

            this.setMouseEnabled(true);

            return true;
        },

        initBackgroundImage : function (size) {
            this.backgroundImage = cc.Sprite.create("res/background.png");
            this.backgroundImage.setAnchorPoint(cc.p(0.5, 0.465));
            this.backgroundImage.setPosition(cc.p(size.width / 2, size.height / 2));

            this.lazyLayer.addChild(this.backgroundImage, 0);
        },

        initMap : function () {
            this.map = cc.TMXTiledMap.create("res/level-001.tmx");
            this.mapObjects = this.map.getObjectGroup("Objects");

            this.lazyLayer.addChild(this.map);
        },

        initHeart : function () {
            var Heart = cc.Sprite.extend({
                _basicAnimation : [],

                ctor : function () {
                    this._super();

                    var heartTexture = cc.TextureCache.getInstance().addImage("res/heart.png");

                    this._basicAnimation.push(cc.SpriteFrame.createWithTexture(heartTexture, cc.rect(0, 0, 80, 80)));
                    this._basicAnimation.push(cc.SpriteFrame.createWithTexture(heartTexture, cc.rect(80, 0, 80, 80)));
                    this._basicAnimation.push(cc.SpriteFrame.createWithTexture(heartTexture, cc.rect(160, 0, 80, 80)));
                    this._basicAnimation.push(cc.SpriteFrame.createWithTexture(heartTexture, cc.rect(240, 0, 80, 80)));
                    this._basicAnimation.push(cc.SpriteFrame.createWithTexture(heartTexture, cc.rect(320, 0, 80, 80)));
                },

                onEnter:function () {
                    this._super();
                    this.animate();
                },

                animate : function () {
                    this.stopAllActions();
                    this.runAction(
                        cc.RepeatForever.create(
                            cc.Animate.create(
                                cc.Animation.create(this._basicAnimation, 0.25)
                            )
                        )
                    );
                }
            });

            var heart = new Heart();

            heart.setPosition(cc.p(280, 440));

            this.addChild(heart, 49);
        },

        onMouseDown:function (e) {
            var loc = e.getLocation();
            var weapons = weaponFactory.getChildren();

            for (var i = 0, len = weapons.length; i < len; ++i) {
                var wp = weapons[i],
                    size = wp.getContentSize(),
                    position = wp.getPosition();

                var x = position.x,
                    y = position.y,
                    w = size.width,
                    h = size.height;

                if (loc.x >= x - w / 2 && loc.x <= x + w / 2) {
                    if (loc.y >= y / 2 && loc.y <= y + h / 2) {
                        selectedElement = wp;
                        selectedElementIndex = i;
                        selectedElementSprite = Tw.Weapons.WeaponGenerator.generateWeapon(loc, wp.tid);
                        this.addChild(selectedElementSprite, 99);
                        break;
                    }
                }
            }
        },

        onMouseDragged:function (e) {
            var loc = e.getLocation();

            this.isDragging = true;

            if (selectedElementSprite) {
                selectedElementSprite.setPosition(loc.x, loc.y);
            }
        },

        onMouseUp:function (e) {
            if (!this.isDragging) {
                this.removeChild(selectedElementSprite);
                selectedElementSprite = null;
                selectedElementIndex = null;
                selectedElement = null;
                return;
            }

            this.isDragging = false;

            var loc = e.getLocation();

            if (selectedElement) {
                for (var i = 0, len = dropAreas.length; i < len; ++i) {
                    var da = dropAreas[i],
                        x = da.x,
                        y = da.y,
                        w = da.width,
                        h = da.height;

                    if (loc.x >= x && loc.x <= x + w) {
                        if (loc.y >= y && loc.y <= y + h) {
                            // unleash the bomb
                            var portalId = getPortalIdFromName(da.portal);
                            // remove the sprite from the queue & update the weapon queue
                            villainFactories[portalId].removeAllChildrenByWeaponAndPortalId(selectedElement, portalId);
                            weaponFactory.removeChild(selectedElement);
                            break;
                        }
                    }
                }
            }

            selectedElementIndex = null;
            this.removeChild(selectedElementSprite);
        }
    });

    var MyScene = cc.Scene.extend({
        onEnter :function () {
            this._super();

            layer = new MyLayer();

            this.addChild(layer);

            layer.init();

            setupMapObjects(layer.mapObjects.getObjects(), this);
        }
    });

    return {
        layers : [
            MyLayer
        ],
        scenes : [
            MyScene
        ],
        clear : function () {
            for (var i = 0, len = villainFactories.length; i < len; ++i) {
                villainFactories[i].removeAllChildren();
            }

            defeatedFactories = 0;
            stoppedFactories = 0;
        },
        factoryStopped : function (portalId, villainsCount) {
            if (stoppedFactories < villainFactories.length) {
                ++stoppedFactories;
                return;
            }
        },
        factoryDefeated : function (portalId) {
            ++defeatedFactories;

            if (defeatedFactories !== villainFactories.length) {
                return;
            }

            if (stoppedFactories !== villainFactories.length) {
                return;
            }

            Tw.endGame(true);
        }
    };
}(window.cc, window.Tw));

Tw.Levels['000'].App.nextLevel = Tw.Levels['001'].App;
