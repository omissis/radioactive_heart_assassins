_.ns('Tw.Actors');

Tw.Actors.Drug = function (position, options) {
    var Villain = new Tw.Actors.Villain(position, 'villain-drug', options);

    Villain.tid = Tw.Actors.T_DRUG;
    Villain.attack = 1;
    Villain.defense = 1;
    Villain.speed = 0.5;

    return Villain;
};

Tw.Actors.T_DRUG = 1;
