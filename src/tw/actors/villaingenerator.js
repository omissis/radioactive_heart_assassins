_.ns('Tw.Actors');

(function (Tw, cc) {
    "use strict";

    Tw.Actors.VillainGenerator = new Tw.Common.Generator(
        [
            Tw.Actors.T_BACTERIUM,
            Tw.Actors.T_DRUG,
            Tw.Actors.T_RADIOACTIVE,
            Tw.Actors.T_VIRUS
        ],
        {
            maxElementsPerWave : 1
        }
    );

    Tw.Actors.VillainGenerator.getVillainByType = function (type) {
        if (Tw.Actors.T_BACTERIUM === type) {
            return Tw.Actors.Bacterium;
        }

        if (Tw.Actors.T_DRUG === type) {
            return Tw.Actors.Drug;
        }

        if (Tw.Actors.T_RADIOACTIVE === type) {
            return Tw.Actors.Radioactive;
        }

        if (Tw.Actors.T_VIRUS === type) {
            return Tw.Actors.Virus;
        }

        throw "given villain type '" + type + "' does not exists";
    };

    Tw.Actors.VillainGenerator.generateVillain = function (obj, type, options) {
        var element = Tw.Actors.VillainGenerator.generateElement(type),
            VillainCtor = Tw.Actors.VillainGenerator.getVillainByType(element[0]),
            startingPoint = cc.p(obj.x, obj.y);

        return new VillainCtor(startingPoint, options);
    };

    Tw.Actors.VillainGenerator.generateVillainAndTimeout = function (obj, options) {
        var element = Tw.Actors.VillainGenerator.generateElement(),
            VillainCtor = Tw.Actors.VillainGenerator.getVillainByType(element[0]),
            startingPoint = cc.p(
                obj.x + Tw.Common.Config.halfCellWidth,
                obj.y + Tw.Common.Config.halfCellHeight
            );

        return {
            villain : new VillainCtor(startingPoint, options),
            timeout : element[1]
        };
    };
}(window.Tw, window.cc));
