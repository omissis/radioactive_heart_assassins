_.ns('Tw.Actors');

Tw.Actors.Bacterium = function (position, options) {
    var Villain = new Tw.Actors.Villain(position, 'villain-bacterium', options);

    Villain.tid = Tw.Actors.T_BACTERIUM;
    Villain.attack = 1;
    Villain.defense = 1;
    Villain.speed = 1;

    return Villain;
};

Tw.Actors.T_BACTERIUM = 0;
