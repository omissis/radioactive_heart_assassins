_.ns('Tw.Actors');

Tw.Actors.Radioactive = function (position, options) {
    var Villain = new Tw.Actors.Villain(position, 'villain-radioactive', options);

    Villain.tid = Tw.Actors.T_RADIOACTIVE;
    Villain.attack = 3;
    Villain.defense = 1;
    Villain.speed = 2;

    return Villain;
};

Tw.Actors.T_RADIOACTIVE = 2;
