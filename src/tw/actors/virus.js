_.ns('Tw.Actors');

Tw.Actors.Virus = function (position, options) {
    var Villain = new Tw.Actors.Villain(position, 'villain-virus', options);

    Villain.tid = Tw.Actors.T_VIRUS;
    Villain.attack = 1;
    Villain.defense = 3;
    Villain.speed = 0.75;

    return Villain;
};

Tw.Actors.T_VIRUS = 3;
