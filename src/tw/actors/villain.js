_.ns('Tw.Actors');

Tw.Actors.Villain = function (position, image, options) {
    var Villain = cc.Sprite.extend({
        tid : null,
        attack : 1,
        defense : 1,
        speed : 1,
        portalId : null,
        _basicAnimation : [],

        ctor : function () {
            this._super();

            var villainTexture = cc.TextureCache.getInstance().addImage(Tw.Common.Resources.getImage(image).src);

            this._basicAnimation.push(cc.SpriteFrame.createWithTexture(villainTexture, cc.rect(0, 0, 32, 32)));
            this._basicAnimation.push(cc.SpriteFrame.createWithTexture(villainTexture, cc.rect(32, 0, 32, 32)));
            this._basicAnimation.push(cc.SpriteFrame.createWithTexture(villainTexture, cc.rect(64, 0, 32, 32)));
            this._basicAnimation.push(cc.SpriteFrame.createWithTexture(villainTexture, cc.rect(96, 0, 32, 32)));
            this._basicAnimation.push(cc.SpriteFrame.createWithTexture(villainTexture, cc.rect(128, 0, 32, 32)));
        },

        onEnter:function () {
            this._super();
            this.animate();
        },

        animate : function () {
            this.stopAllActions();
            this.runAction(
                cc.RepeatForever.create(
                    cc.Animate.create(
                        cc.Animation.create(this._basicAnimation, 0.25)
                    )
                )
            );
        }
    });

    options = options || {};

    var ret = new Villain();

    ret.initWithFile(Tw.Common.Resources.getImage(image).src);
    ret.setPosition(position);
    ret.setTag('villain');
    ret.portalId = options.portalId;

    return ret;
};
