_.ns('Tw.Actors');

(function (Tw, setTimeout, clearTimeout, _) {
    "use strict";

    Tw.Actors.VillainFactory = function (cc, Tw, scene, obj, portalId) {
        if (!cc)    throw "no cocos2d";
        if (!Tw)    throw "no Tw";
        if (!scene) throw "no scene";
        if (!obj)   throw "no obj";

        var villainsTimeouts = [],
            villainTags = {},
            finalPoint = null,
            animations = [],
            maxVillains = 0;

        var getVillainsCount = function () {
                var count = 0;

                _.each(villainTags, function (value, key) {
                    count += value;
                });

                return count;
            },
            removeVillain = function (child) {
                scene.removeChild(child);

                --villainTags[child.tid];

                if (villainTags.tid > 0) {
                    return;
                }

                if (getVillainsCount() > 0) {
                    return;
                }

                Tw.currentLevel.factoryDefeated(portalId);
            },
            stopFactory = function () {
                for (var i = 0, len = villainsTimeouts.length; i < len; ++i) {
                    clearTimeout(villainsTimeouts[i]);
                    villainsTimeouts[i] = null;
                }
                villainsTimeouts = [];

                // I assume VillainFactory has the same id as portal, even though it would be better to set its own.

                Tw.currentLevel.factoryStopped(portalId, getVillainsCount());
            },
            createEnemy = function (finalPositionCallback, timeout) {
                timeout = timeout || 1;

                villainsTimeouts.push(
                    setTimeout(function (scene) {
                        var vt = Tw.Actors.VillainGenerator.generateVillainAndTimeout(obj, {portalId : portalId}),
                            villainAction = null,
                            animationQueue = [];

                        scene.addChild(vt.villain, 1);

                        var tag = vt.villain.tid;
                        villainTags[tag] = _.isNumber(villainTags[tag]) ? villainTags[tag] + 1 : 1;

                        for (var i = 0, len = animations.length; i < len; ++i) {
                            animationQueue[i] = cc.MoveTo.create(
                                animations[i][0] * vt.villain.speed,
                                animations[i][1]
                            );
                        }

                        animationQueue[i] = cc.MoveTo.create(1.0, finalPoint);

                        villainAction = vt.villain.runAction(cc.Sequence.create(animationQueue));
                        vt.villain.schedule(function () {
                            var position = this.getPosition();
                            if (position.x === finalPoint.x && position.y === finalPoint.y) {
                                finalPositionCallback(vt.villain.attack);
                                vt.villain.unscheduleAllCallbacks();
                                removeVillain(vt.villain);
                            }
                        }, 0.1);

                        if (getVillainsCount() < maxVillains) {
                            createEnemy(finalPositionCallback, vt.timeout);
                        } else {
                            Tw.currentLevel.factoryStopped(portalId, getVillainsCount());
                        }
                    }, timeout * 1000, scene)
                );
            };

        animations = Tw.Common.Factory.getElementFromPortalAnimation(obj);

        return {
            start : function (maxVillainsNumber, finalPosition, finalPositionCallback) {
                maxVillains = maxVillainsNumber;
                finalPoint = cc.p(finalPosition.x, finalPosition.y);
                createEnemy(finalPositionCallback);
            },
            stop : stopFactory,
            removeAllChildren : function () {
                var child = null;

                _.each(villainTags, function (value, key) {
                    while ((child = scene.getChildByTag(key))) {
                        removeVillain(child);
                    }
                });
            },
            removeAllChildrenByWeaponAndPortalId : function (weapon, portalId) {
                var children = scene.getChildren();

                for (var i = 0; i < children.length; ++i) {
                    if (_.isVoid(children[i])) { console.log(i, len); continue; }  //TODO check why sometimes this happens
                    if (_.isVoid(children[i].tid)) continue;
                    if (_.isVoid(children[i].portalId)) continue;

                    if (children[i].tid !== weapon.against) continue;
                    if (children[i].portalId !== portalId) continue;

                    removeVillain(children[i]);
                }
            },
            getVillainsCount : getVillainsCount
        };
    };
}(window.Tw, window.setTimeout, window.clearTimeout, window._));
