_.ns('Tw.Actors');

Tw.Actors.Heart = function (points) {
    var _points = points || Number.MAX_VALUE;

    return {
        onHit : function (points) {
            _points -= points;
            console.log('hit! remaining points: ', _points);
            if (_points <= 0) Tw.endGame(false);
        },
        ctor : function (points) {
            _points = points;
        },
        getPoints : function () {
            return _points;
        },
        setPoints : function (points) {
            _points = points;
        },
        addPoints : function (points) {
            _points += points;
            if (_points <= 0) Tw.endGame(false);
        },
        removePoints : function (points) {
            _points -= points;
            if (_points <= 0) Tw.endGame(false);
        }
    };
};
