/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Base class and functions for Radioactive Heart Assassins library
 *
 * @author Claudio Beatrice <omissis@10warp.com>
 */

var Tw = (function (_, cc) {
    "use strict";

    var defaultOptions = {
            COCOS2D_DEBUG : 2, //0 to turn debug off, 1 for basic debug, and 2 for full debug
            box2d : false,
            chipmunk : false,
            showFPS : true,
            frameRate : 60,
            tag : 'gameCanvas', //the dom etwent to run cocos2d on
            engineDir : '../lib/cocos2d/',
            //SingleEngineFile:'',
            appFiles : [
                'src/tw/resources.js',

                'src/tw/common/config.js',
                'src/tw/common/resources.js',
                'src/tw/common/generator.js',
                'src/tw/common/factory.js',

                'src/tw/weapons/weapon.js',
                'src/tw/weapons/antibodies.js',
                'src/tw/weapons/medicin.js',
                'src/tw/weapons/oxygen.js',
                'src/tw/weapons/vaccine.js',
                'src/tw/weapons/weapongenerator.js',
                'src/tw/weapons/weaponfactory.js',

                'src/tw/actors/heart.js',
                'src/tw/actors/villain.js',
                'src/tw/actors/bacterium.js',
                'src/tw/actors/drug.js',
                'src/tw/actors/radioactive.js',
                'src/tw/actors/virus.js',
                'src/tw/actors/villaingenerator.js',
                'src/tw/actors/villainfactory.js',

                'src/tw/levels/000/app.js',
                'src/tw/levels/001/app.js',
                'src/tw/levels/loser/app.js',
                'src/tw/levels/winner/app.js'
            ]
        };

    var onDomContentLoadedListener = function () {
        window.addEventListener('DOMContentLoaded', function () {
            //first load engine file if specified
            var s = window.document.createElement('script');

            /*********Delete this section if you have packed all files into one*******/
            if (Tw.options.SingleEngineFile && !Tw.options.engineDir) {
                s.src = Tw.options.SingleEngineFile;
            } else if (Tw.options.engineDir && !Tw.options.SingleEngineFile) {
                s.src = Tw.options.engineDir + 'platform/jsloader.js';
            } else {
                window.alert('You must specify either the single engine file OR the engine directory in "cocos2d.js"');
            }
            /*********Delete this section if you have packed all files into one*******/

            //s.src = 'Packed_Release_File.js'; //IMPORTANT: Un-comment this line if you have packed all files into one

            window.document.body.appendChild(s);
            window.document.ccConfig = Tw.options;
            s.id = 'cocos2d-html5';
            //else if single file specified, load singlefile
        });
    };

    return {
        options : {},

        currentLevel : {},

        init : function (options) {
            this.options = _.extend(options || {}, defaultOptions);

            onDomContentLoadedListener();
        },

        /**
         * Get the a constant speed value given two points
         */
        getSpeed : function (newCoords, oldCoords, multiplier) {
            multiplier = multiplier || 1.0;

            var xDiff = newCoords.x - oldCoords.x;
            if (xDiff !== 0) {
                return Math.abs(xDiff) * multiplier;
            }

            var yDiff = newCoords.y - oldCoords.y;
            if (yDiff !== 0) {
                return Math.abs(yDiff) * multiplier;
            }

            return 0.0;
        },
        gotoNextLevel : function () {
            Tw.currentLevel = Tw.currentLevel.nextLevel;
            window.cc.Director.getInstance().replaceScene(new Tw.currentLevel.scenes[0]());
        },
        gotoFirstLevel : function () {
            Tw.currentLevel = Tw.Levels['000'].App;
            window.cc.Director.getInstance().replaceScene(new Tw.currentLevel.scenes[0]());
        },
        endGame : function (won) {
            if (Tw.currentLevel.clear) {
                Tw.currentLevel.clear();
            }

            window.cc.Director.getInstance().replaceScene(
                new Tw.Levels[won ? 'Winner' : 'Loser'].App.scenes[0]()
            );
        }
    };
}(_, window));
