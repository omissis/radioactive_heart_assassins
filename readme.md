The Radioactive Heart Assassins Project
=======================================

We are going to be the villain (a evil doctor)

The weapons we can make use of could be:
* bacterias (standard ones; att: 1, def: 1, spd: 1)
* drugs (faster; att: 1, def: 1, spd: 2)
* radiations (slower, more powerful; att: 3, def: 1, spd: 0.5)
* viruses (harder to kill; att: 1, def: 3, spd: 1.5))

The defences heart has could be:
* anti-bacteria (against bacterias)
* vaccine (against-viruses)
* antibodies (agains bacterias and viruses)
* oxygen (against drugs)
* medicins (against radiations)

Proposed names:
* Heart Incident
* Heart Knight
* Topsy-Turvy Heart Attack
* Topsy-Turvy Heart Crisis
* Fatal Heart Insurrection
* Radioactive Heart Assassins
