TODO
====

Assets(animations would be a plus):
* sounds and musics (free ones)
* ui elements (menu, elements, buttons, icons)
* more levels
* favicon

Code:
* Animations

Nice to have:
* combined enemies that can be killed by using two different weapons on them
* mobile application (iOS, Android, WinPhone)
* Topsy-Turvy PIZZA! :)



DONE
====

Assets:
* enemies
* weapons
* hearts (at least one)
* backgrounds (at least one)

Code:
* level management (if we have time)
* create the HUD for weapons
* refactor villain factory to use the new common.factory and common.generator
* Fix villain speed since now it's different depending on the length of the animation
* create a villain factory(very basic: random generation, recommended: file loading)
* way to keep the score (so: enemy "collision" with the heart)
* detect and deal with the end of the match
* create a weapons factory
* create the weapons interactions
