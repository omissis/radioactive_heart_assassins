(function (window) {
    "use strict";

    var cc = window.cc,
        Tw = window.Tw,
        document = window.document;

    var Cocos2dApp = cc.Application.extend({
        config : window.document.ccConfig,
        ctor : function (scene) {
            this._super();
            this.startScene = scene;
            cc.COCOS2D_DEBUG = this.config.COCOS2D_DEBUG;
            cc.initDebugSetting();
            cc.setup(this.config.tag);
            cc.Loader.getInstance().onloading = function () {
                cc.LoaderScene.getInstance().draw();
            };
            cc.Loader.getInstance().onload = function () {
                cc.AppController.shareAppController().didFinishLaunchingWithOptions();
            };
            cc.Loader.getInstance().preload(Tw.Common.Resources.getAll());
        },
        applicationDidFinishLaunching : function () {
            // initialize director
            var director = cc.Director.getInstance();

            // enable High Resource Mode(2x, such as iphone4) and maintains low resource on other devices.
            // director->enableRetinaDisplay(true);

            // turn on display FPS
            director.setDisplayStats(this.config.showFPS);

            // set FPS. the default value is 1.0/60 if you don't call this
            director.setAnimationInterval(1.0 / this.config.frameRate);

            // create a scene. it's an autorelease object

            // run
            director.runWithScene(new this.startScene());

            return true;
        }
    });

    var level = Tw.Levels['000'].App;

    var myApp = new Cocos2dApp(level.scenes[0]);

    Tw.currentLevel = level;
}(window));
